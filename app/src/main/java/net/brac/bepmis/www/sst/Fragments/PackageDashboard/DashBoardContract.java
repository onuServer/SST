package net.brac.bepmis.www.sst.Fragments.PackageDashboard;

import net.brac.bepmis.www.sst.Fragments.PackageDashboard.DashBoardClassCollection.AttendanceListParameters;

import java.util.List;

public interface DashBoardContract {
    interface MainView{
        public void inflateAttendanceList(List<AttendanceListParameters> parameters);
        public void progressUpdate(int attendanceValue, int visited, int environment);

    }
    interface MainPresenter{
        public void actOnAttendanceList();
        public void actOnProgressUpdate();
    }
}
