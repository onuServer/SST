package net.brac.bepmis.www.sst.Fragments.PackageDashboard.DashBoardClassCollection;

public class AttendanceListParameters {
    public String schoolName;
    public String schoolCode;
    public String grade;
    public int attendanceParcent;

    public AttendanceListParameters(String schoolName, String schoolCode, String grade, int attendanceParcent) {
        this.schoolName = schoolName;
        this.schoolCode = schoolCode;
        this.grade = grade;
        this.attendanceParcent = attendanceParcent;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolCode() {
        return schoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getAttendanceParcent() {
        return attendanceParcent;
    }

    public void setAttendanceParcent(int attendanceParcent) {
        this.attendanceParcent = attendanceParcent;
    }
}
