package net.brac.bepmis.www.sst.Fragments.PackageCheackList;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import net.brac.bepmis.www.sst.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckListFragment extends Fragment {

    private TextView envList;


    public CheckListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_check_list, container, false);
        envList=view.findViewById(R.id.envList);
        envList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Pop-up will come",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}
