package net.brac.bepmis.www.sst.Fragments.PackageDashboard;

import android.os.Handler;

import net.brac.bepmis.www.sst.Fragments.PackageDashboard.DashBoardClassCollection.AttendanceListParameters;

import java.util.ArrayList;
import java.util.List;

public class DashBoardPresenter implements DashBoardContract.MainPresenter{

    private DashBoardContract.MainView view;
    private Handler handler = new Handler();
    private int averageAttendance = 0;
    private int averageVisited = 0;
    private int averageEnvironment = 0;
    private int curStatus = 0;
    private int attendance=0, visited=0, environment=0;

    public DashBoardPresenter(DashBoardContract.MainView view){
        this.view=view;
    }

    @Override
    public void actOnAttendanceList() {
        List<AttendanceListParameters> parametersList=new ArrayList<>();
        //all calculations will be here

        view.inflateAttendanceList(parametersList);
    }

    @Override
    public void actOnProgressUpdate() {
        averageAttendance= 80;
        averageEnvironment= 90;
        averageVisited=45;

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (curStatus <= averageAttendance) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (curStatus<=averageAttendance){
                                attendance=curStatus;
                            }
                            if (curStatus<=averageVisited){
                                visited=curStatus;

                            }if (curStatus<=averageEnvironment){
                                environment=curStatus;
                            }
                            view.progressUpdate(attendance,visited,environment);
                        }
                    });
                    try {
                        Thread.sleep(70);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (averageAttendance > 0)// added later
                        curStatus++;
                }
            }
        }).start();
    }
}
