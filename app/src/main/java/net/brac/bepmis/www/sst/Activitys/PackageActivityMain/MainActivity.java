package net.brac.bepmis.www.sst.Activitys.PackageActivityMain;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import net.brac.bepmis.www.sst.Fragments.PackageCheackList.CheckListFragment;
import net.brac.bepmis.www.sst.Fragments.PackageNotification.NotificationFragment;
import net.brac.bepmis.www.sst.Fragments.PackageAttendanceFrag.AttendanceFragment;
import net.brac.bepmis.www.sst.Fragments.PackageDashboard.DashBoard;
import net.brac.bepmis.www.sst.Fragments.PackageSchoolList.SchoolListFragment;
import net.brac.bepmis.www.sst.Fragments.PackageTypeCheckList.TypeCheckListFragment;
import net.brac.bepmis.www.sst.Fragments.PackageProfile.UserProfileFragment;
import net.brac.bepmis.www.sst.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DashBoard.DashboardListener,
        TypeCheckListFragment.CheckListListener{

    private DrawerLayout drawerLayout;
    private String TAG = "MainActivity";
    private ImageView menuBTN,homeBTN,profileBTN,notificationBTN,refreshBTN;
    //fragment operation

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //toolbar operation
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();

        menuBTN=toolbar.findViewById(R.id.toolMenuBtn);
        homeBTN=toolbar.findViewById(R.id.toolHomeBtn);
        profileBTN=toolbar.findViewById(R.id.toolProfileBtn);
        notificationBTN=toolbar.findViewById(R.id.toolNotificationBtn);
        refreshBTN=toolbar.findViewById(R.id.toolRefreshBtn);

        menuBTN.setOnClickListener(this);
        homeBTN.setOnClickListener(this);
        profileBTN.setOnClickListener(this);
        notificationBTN.setOnClickListener(this);
        refreshBTN.setOnClickListener(this);

        //drawer operation
        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();
                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        //fragment fabrications operation
        Fragment fragment=new DashBoard();
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer,fragment);
        transaction.addToBackStack(null);;
        transaction.commit();

    }
//onclick event for all toolbar menu

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        switch (v.getId()){
            case R.id.toolMenuBtn:
                Log.i(TAG,"click on menu");
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.toolHomeBtn:
                Log.i(TAG,"click on home");
                Fragment fragment=new DashBoard();
                transaction.replace(R.id.fragmentContainer,fragment);
                break;
            case R.id.toolProfileBtn:
                Log.i(TAG,"click on profile");
                Fragment profileFragment = new UserProfileFragment();
                transaction.replace(R.id.fragmentContainer,profileFragment);
                break;
            case R.id.toolNotificationBtn:
                Log.i(TAG,"click on notification");
                Fragment notificationFragment =new NotificationFragment();
                transaction.replace(R.id.fragmentContainer,notificationFragment);

                break;
            case R.id.toolRefreshBtn:
                Log.i(TAG,"click on refresh");
                Toast.makeText(this,"This button will work in background for syncing all data",Toast.LENGTH_LONG).show();
                break;
        }
        transaction.addToBackStack(null);;
        transaction.commit();
    }

    @Override
    public void toAttendance() {
        Fragment fragment=new AttendanceFragment();
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer,fragment);
        transaction.addToBackStack(null);;
        transaction.commit();
    }

    @Override
    public void toSchoolList() {
        Fragment fragment=new SchoolListFragment();
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer,fragment);
        transaction.addToBackStack(null);;
        transaction.commit();
    }

    @Override
    public void toCheckListType() {
        Fragment fragment=new TypeCheckListFragment();
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer,fragment);
        transaction.addToBackStack(null);;
        transaction.commit();
    }

    @Override
    public void toCheckList() {
        Fragment fragment=new CheckListFragment();
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer,fragment);
        transaction.addToBackStack(null);;
        transaction.commit();
    }


}
