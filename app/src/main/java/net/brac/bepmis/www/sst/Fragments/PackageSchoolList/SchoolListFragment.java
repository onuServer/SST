package net.brac.bepmis.www.sst.Fragments.PackageSchoolList;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import net.brac.bepmis.www.sst.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SchoolListFragment extends Fragment {


    public SchoolListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_school_list, container, false);
    }

}
