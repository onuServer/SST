package net.brac.bepmis.www.sst.Fragments.PackageDashboard;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.brac.bepmis.www.sst.Fragments.PackageDashboard.DashBoardClassCollection.AttendanceListParameters;
import net.brac.bepmis.www.sst.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class DashBoard extends Fragment implements DashBoardContract.MainView{

    //UI elements
    private TextView monthPicker, attendanceBTN,schoolBTN,checkListBTN,attendancePercent,visitedPercent,environmentPresent;
    private ProgressBar attendanceProgress, visitedProgress, environmentProgress;
    private RecyclerView poDashList;
    //calender
    private Calendar calendar;
    private int month, year, day;
    //interface
    DashboardListener dashboardListener;

    private DashBoardPresenter presenter;
    private String TAG="DashBoard";


    public DashBoard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //presenter inertial
        presenter = new DashBoardPresenter(this);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dash_board, container, false);
        //button
        monthPicker = view.findViewById(R.id.monthPicker);
        attendanceBTN=view.findViewById(R.id.attendanceBTN);
        schoolBTN=view.findViewById(R.id.schoolBTN);
        checkListBTN=view.findViewById(R.id.checkListBTN);
        //progress
        attendanceProgress=view.findViewById(R.id.attendanceCircularProgressBar);
        visitedProgress=view.findViewById(R.id.visitedCircularProgressBar);
        environmentProgress=view.findViewById(R.id.environmentCircularProgressBar);
        //recyclerView
        poDashList=view.findViewById(R.id.poDashList);
        dashboardListener= (DashboardListener) getActivity();

        //calender
        calendar=Calendar.getInstance();
        year=calendar.get(Calendar.YEAR);
        month=calendar.get(Calendar.MONTH);
        day=calendar.get(Calendar.DAY_OF_MONTH);

        //percent text
        attendancePercent=view.findViewById(R.id.attendancePercent);
        visitedPercent=view.findViewById(R.id.visitedPercent);
        environmentPresent=view.findViewById(R.id.environmentPercent);

        //all click events
        monthPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");

                                calendar.set(i1,i2);
                                String date = sdf.format(calendar.getTime());
                                Log.i(TAG,date);
                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
        attendanceBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Attendance clicked",Toast.LENGTH_SHORT).show();
                dashboardListener.toAttendance();
            }
        });
        schoolBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dashboardListener.toSchoolList();
            }
        });
        checkListBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dashboardListener.toCheckListType();
            }
        });

        presenter.actOnAttendanceList();
        presenter.actOnProgressUpdate();
        return view;
    }

    @Override
    public void inflateAttendanceList(List<AttendanceListParameters> parameters) {
        Toast.makeText(getContext(),"Adapter CAlled",Toast.LENGTH_LONG).show();
    }

    @Override
    public void progressUpdate(int attendance, int visited, int environment) {
        attendanceProgress.setProgress(attendance);
        attendancePercent.setText(attendance+"%");

        visitedProgress.setProgress(visited);
        visitedPercent.setText(visited+"%");

        environmentProgress.setProgress(environment);
        environmentPresent.setText(environment+"%");
    }

    public interface DashboardListener{
        public void toAttendance();
        public void toSchoolList();
        public void toCheckListType();
    }

}
