package net.brac.bepmis.www.sst.Fragments.PackageTypeCheckList;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.brac.bepmis.www.sst.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TypeCheckListFragment extends Fragment {
    private TextView envType;
    private CheckListListener checkListListener;


    public TypeCheckListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_check_list_type, container, false);
        envType=view.findViewById(R.id.envType);
        checkListListener= (CheckListListener) getActivity();
        envType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkListListener.toCheckList();
            }
        });
        return view;
    }

    public interface CheckListListener{
        public void toCheckList();
    }

}
