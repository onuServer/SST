package net.brac.bepmis.www.sst.Activitys.PackageSplash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import net.brac.bepmis.www.sst.Activitys.PackageActivityMain.MainActivity;
import net.brac.bepmis.www.sst.R;

public class SplashWelcome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_welcome);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashWelcome.this, MainActivity.class));
            }
        },1500);
    }
}
