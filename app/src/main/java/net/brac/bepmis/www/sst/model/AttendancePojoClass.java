package net.brac.bepmis.www.sst.model;

public class AttendancePojoClass {
    private int index;

    public AttendancePojoClass(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
